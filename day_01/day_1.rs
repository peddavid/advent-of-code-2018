use std::fs;
use std::collections::HashSet;

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read file");
    let lines = data.split("\n");

    let mut set = HashSet::new();
    let numbers = lines
        .map(|line| line.chars().filter(|c| *c != '+').collect::<String>().trim().parse::<i32>())
        .filter(|result| result.is_ok())
        .map(|result| result.unwrap())
        .collect::<Vec<i32>>();
    let mut sum = 0;
    set.insert(sum);

    loop {
        for number in numbers.iter() {
            sum += number;
            if !set.insert(sum) {
                println!("{}", sum);
                return;
            }
        }
    }
}