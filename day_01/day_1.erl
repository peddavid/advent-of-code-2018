-module(day_1).
-export([run/0, test/0]).

run() ->
    {ok, S} = file:open("input", read),
    Line = io:get_line(S, ''),
    Data = string:strip(Line, right, $\n),
    Number = list_to_integer(Line),
    io:format("~p~n", [Number]).

test() ->
    {ok, Binary} = file:read_file("input"),
    binary_to_list(Binary).
    % Binary.
