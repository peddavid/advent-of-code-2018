"use strict";
exports.__esModule = true;
function greeter(person) {
    return "Hello, " + person;
}
var user = "Jane User";
console.log(greeter(user));
var fs = require("fs");
// fs.readFileSync('input', 'utf8')
//     .split(/\r\n|\r|\n/)
//     .map(toEntry)
// toEntry(fs.readFileSync('input', 'utf8')
//     .split(/\r\n|\r|\n/)[0])
// toEntry(fs.readFileSync('input', 'utf8')
//     .split(/\r\n|\r|\n/)[1])
toEntry(fs.readFileSync('input', 'utf8')
    .split(/\r\n|\r|\n/)[2]);
toEntry(fs.readFileSync('input', 'utf8')
    .split(/\r\n|\r|\n/)[3]);
toEntry(fs.readFileSync('input', 'utf8')
    .split(/\r\n|\r|\n/)[13]);
var MDate = /** @class */ (function () {
    function MDate(month, day) {
        this.month = month;
        this.day = day;
    }
    return MDate;
}());
function toEntry(line) {
    var entryPattern = /\[(.*)\] (falls asleep|wakes up|Guard #(\d+) begins shift)/;
    var _a = entryPattern.exec(line) || ["", "", "", ""], dateString = _a[1], stateChange = _a[2], id = _a[3];
    var datePattern = /(\d\d\d\d)-(\d\d)-(\d\d) (\d\d):(\d\d)/;
    var _b = datePattern.exec(dateString), month = _b[2], day = _b[3], hour = _b[4], minute = _b[5];
    console.log(new MDate(parseInt(month), parseInt(day)));
    console.log(stateChange);
    console.log(id);
    return {};
}
var Entry = /** @class */ (function () {
    function Entry() {
    }
    return Entry;
}());
