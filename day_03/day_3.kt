import java.io.File

fun main(args: Array<String>) {
    println("Hello World")
    val file = File(if (args.size == 1) args[0] else "input")
    val lines = file.readLines()

    print("Star 1: ")
    val claims = lines.map { claimFromString(it) }

    val claimCountForSpace = claims.flatMap { it.toUsedSquareRoots(1000) }.groupBy { it }
    val claimsOccupyingSameSpace = claimCountForSpace.filterValues { it.size > 1 }
    val countOfSpacesWithMoreThanOneClaim = claimsOccupyingSameSpace.size
    println(countOfSpacesWithMoreThanOneClaim)

    print("Star 2: ")
    val nonInterceptingClaims = claims.filter { claim ->
        val usedSquares = claim.toUsedSquareRoots(1000)
        usedSquares.all { square -> claimCountForSpace[square]!!.size == 1 }
    }
    println(nonInterceptingClaims)
}

fun claimFromString(str: String): Claim {
    val removeWhiteSpaces = str.replace(" ", "")
    val (idStr, dimensionsStr) = removeWhiteSpaces.split("@")
    val id = idStr.trim('#')
    val (originStr, sizeStr) = dimensionsStr.split(":")
    val (x, y) = originStr.split(",")
    val (width, height) = sizeStr.split("x")
    return Claim(id.toInt(), x.toInt(), y.toInt(), width.toInt(), height.toInt())
}

data class Claim(val id: Int, val x: Int, val y: Int, val width: Int, val height: Int) {
    fun toUsedSquareRoots(size: Int) : List<Int> {
        val xRange = (x..(x + width - 1)).toList()
        return (y..(y + height - 1)).flatMap { thisY ->
            xRange.map { (thisY * size + it) }
        }
    }
}

#TODO: Rasterizer :D
